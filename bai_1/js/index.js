const BASE_URL = "https://635f4b22ca0fe3c21a991ec4.mockapi.io";

var idEdited = null;

//render all todos service

function fetchAllTodo() {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todos`,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();
      renderTodoList(res.data);
    })

    .catch(function (err) {
      turnOffLoading();
    });
}
fetchAllTodo();
//remove todos service

function removeTodo(idTodo) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todos/${idTodo}`,
    method: "DELETE",
  })
    .then(function (res) {
      turnOffLoading();
      fetchAllTodo();
    })
    .catch(function (err) {
      turnOffLoading();
    });
}

function addTodo() {
  var data = layThongTinTuForm();
  var newTodo = {
    name: data.name,
    desc: data.desc,
    isComplete: true,
  };
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todos`,
    method: "POST",
    data: newTodo,
  })
    .then(function (res) {
      turnOffLoading();
      fetchAllTodo();
    })

    .catch(function (err) {
      turnOffLoading();
    });
}
function editTodo(idTodo) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todos/${idTodo}`,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();
      document.getElementById("name").value = res.data.name;
      document.getElementById("desc").value = res.data.desc;
      idEdited = res.data.id;
    })
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}
function updateTodo() {
  let data = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/todos/${idEdited}`,
    method: "PUT",
    data: data,
  })
    .then(function (res) {
      console.log("res: ", res);
      fetchAllTodo();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}
