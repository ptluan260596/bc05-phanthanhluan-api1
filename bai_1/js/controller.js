function renderTodoList(todos) {
  var contentHTML = "";
  todos.forEach(function (item) {
    var contentTr = `<tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${item.desc}</td>
        <td>
        <input type="checkbox" ${item.isComplete ? "checked" : ""}/>
        </td>
        <td class="d-flex">
        <button class="btn btn-danger mr-3" onclick="removeTodo(${
          item.id
        })">Delete</button>
        <button class="btn btn-warning" onclick="editTodo(${
          item.id
        })">Edit</button>
        </td>
        </tr>`;
    contentHTML += contentTr;
  });
  document.getElementById("tbody-todos").innerHTML = contentHTML;
}

function turnOnLoading() {
  document.getElementById("loading").style.display = "flex";
}
function turnOffLoading() {
  document.getElementById("loading").style.display = "none";
}
function layThongTinTuForm() {
  var name = document.getElementById("name").value;
  var desc = document.getElementById("desc").value;
  return {
    name: name,
    desc: desc,
  };
}
